# stjude_onshape_signup

Fllow the steps to create an OnShape account:

Go to [this link](https://cad.onshape.com/signup2?basicdataid=882c39dc-b273-4a58-a8f4-38562f02001a&extendeddataid=e49c85b6-e574-4303-a137-259ac45146a1&enable_cookie=check&locale=en_US&approveUser=yes&planid=EDU_YEARLY&referringWwwFormUrl=https:%2F%2Fwww.onshape.com%2Fen%2Fblog%2Fteachers-how-to-set-up-your-onshape-classroom&mostrecentleadsource=https:%2F%2Fwww.onshape.com%2Fen%2Fblog%2Fteachers-how-to-set-up-your-onshape-classroom&contentdownload=https:%2F%2Fwww.onshape.com%2Fen%2Fblog%2Fteachers-how-to-set-up-your-onshape-classroom&galoquaBridgeKey=bb-03-78ad0d76ae-1-ea254e-b0e065&offsiteReferringUrl=https:%2F%2Fwww.google.com%2F&Ipaddress=138.121.143.198&geocountry=Costa%20Rica&geostate=Provincia%20de%20San%20Jose&geocity=San%20Jos%C3%A9&elqCustomerGUID=0debb1ea-1b07-4620-abb2-dcf4636f11f0#_ga=2.186877096.1663319205.1660754210-2063619727.1612047282) 

Use the images below as a guide to fill the application:


![](os_01.png)
---------------------------------------
![](step2.png)
---------------------------------------
![](os_02.png)
---------------------------------------
![](os_03.png)
---------------------------------------
![](os_04.png)
---------------------------------------
![](os_05.png)
---------------------------------------
![](os_06.png)
---------------------------------------
